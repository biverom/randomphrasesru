import os
import json
import shutil

def clear_directory(directory_path):
    # Check if the directory exists
    if not os.path.exists(directory_path):
        return

    # Iterate through all files and directories in the target directory
    for item in os.listdir(directory_path):
        item_path = os.path.join(directory_path, item)

        # Check if the item is a file and delete it
        if os.path.isfile(item_path):
            os.remove(item_path)

        # Check if the item is a directory and delete it recursively
        elif os.path.isdir(item_path):
            shutil.rmtree(item_path)

def read_data_jet_files(directory):
    data_jet_values = []

    for root, _, files in os.walk(directory):
        for filename in files:
            if filename == 'data.jet':
                filepath = os.path.join(root, filename)
                try:
                    with open(filepath, 'r', encoding='utf-8') as file:
                        data = json.load(file)
                        if "fields" in data and len(data["fields"]) >= 2:
                            value = data["fields"][1]["v"]
                            data_jet_values.append(value)
                        else:
                            print(f"Invalid JSON structure in {filepath}")
                except (IOError, json.JSONDecodeError) as e:
                    print(f"Error reading {filepath}: {e}")

    return data_jet_values

def split_into_subarrays(arr, chunk_size):
    return [arr[i:i + chunk_size] for i in range(0, len(arr), chunk_size)]

directory_path = "Drawful2Prompt//"
phrases = read_data_jet_files(directory_path)

with open("words.txt", 'w', encoding='utf-8') as file:
    data = '|'.join(phrases)
    file.write(data)

phraseGroups = split_into_subarrays(phrases, 100)

clear_directory("words//")
for i in range(len(phraseGroups)):
    with open(f"words//{i}.txt", 'w', encoding='utf-8') as file:
        file.write('|'.join(phraseGroups[i]))
